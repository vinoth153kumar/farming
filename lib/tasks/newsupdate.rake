namespace :newsupdate do
  puts "Newsupdating..."

  task news_update: :environment do
    require 'nokogiri'
    require 'open-uri'

	doc = Nokogiri::HTML(open('http://retail.economictimes.indiatimes.com/'))
	Page.find_by_permalink('Latest News').update_attributes(content: doc.to_html)

  end
end
