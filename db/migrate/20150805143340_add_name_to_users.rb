class AddNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :dob,  :date
    add_column :users, :mobile,  :string
    add_column :users, :address,  :string
    add_column :users, :state,  :string
    add_column :users, :country,  :string
  end
end
