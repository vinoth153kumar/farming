class CreateHomeimages < ActiveRecord::Migration
  def change
    create_table :homeimages do |t|
    	t.references :admin_user
    	t.attachment :image

      t.timestamps null: false
    end
  end
end
