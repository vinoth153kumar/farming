class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
    	t.string :name
    	t.string :title
    	t.text :content
    	t.boolean :online,default: true

      t.timestamps null: false
    end
  end
end
