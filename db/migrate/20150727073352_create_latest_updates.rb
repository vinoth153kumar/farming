class CreateLatestUpdates < ActiveRecord::Migration
  def change
    create_table :latest_updates do |t|
    	t.string :title
    	t.string :link
    	t.text :description
    	t.boolean :online,default: true

      t.timestamps null: false
    end
  end
end
