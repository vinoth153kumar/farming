jQuery(document).ready(function(){
	jQuery(window).bind("resize", orange_responsive);
	orange_responsive();
});

var iPhoneVertical = Array(null,320, "/assets/phonevertical.css");
var iPhoneHorizontal = Array(321,750,"/assets/phonehorizontal.css");
var iPadv = Array(751,1023,"/assets/ipadv.css");
var iPad = Array(1024,1200,"/assets/ipad.css");
var dekstop = Array(1201,null,"/assets/desktop.css");

var is_orange_mobile = false;

function orange_responsive(){
	var newWindowWidth = jQuery(window).width();
	if(newWindowWidth >= dekstop[0]){
		if(jQuery("#style-responsive-css").attr("href") == dekstop[2])return;
		jQuery("#style-responsive-css").attr({href : dekstop[2]});
		is_orange_mobile = false;
	}else if(newWindowWidth >= iPad[0] && newWindowWidth <= iPad[1]){
		if(jQuery("#style-responsive-css").attr("href") == iPad[2])return;
		jQuery("#style-responsive-css").attr({href : iPad[2]});
		is_orange_mobile = false;
	}else if(newWindowWidth >= iPadv[0] && newWindowWidth <= iPadv[1]){
		if(jQuery("#style-responsive-css").attr("href") == iPadv[2])return;
		jQuery("#style-responsive-css").attr({href : iPadv[2]});
		is_orange_mobile = false;
	}else if(newWindowWidth >= iPhoneHorizontal[0] && newWindowWidth <= iPhoneHorizontal[1]){
		if(jQuery("#style-responsive-css").attr("href") == iPhoneHorizontal[2])return;
		jQuery("#style-responsive-css").attr({href : iPhoneHorizontal[2]});
		is_orange_mobile = true;
	}else if(newWindowWidth <= iPhoneVertical[1]){
		if(jQuery("#style-responsive-css").attr("href") == iPhoneVertical[2])return;
		jQuery("#style-responsive-css").attr({href : iPhoneVertical[2]});
		is_orange_mobile = true;
	}
}

orange_responsive();