// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require ckeditor/init
//= require owl.carousel
//= require bootstrap-datepicker/core
//= require bootstrap-datepicker/locales/bootstrap-datepicker.es.js
//= require flipclock.min
//= require agritourismo8a54
//= require buddypressc245
//= require jquery-migrate.min1576
//= require orange-themes-responsivead05
//= require theme-scripts5d6b
//= require forem
//= require_tree .

// require 'forecast_io'


$(function() {
  $(window.applicationCache).bind("error", function() {
    console.log("There was an error when loading the cache manifest.");
  });
});
