class PagesController < ApplicationController
  before_filter :load_pages
  respond_to :js, :html, :json

  def show
	  @page = Page.find_by_permalink(params[:id])		
  end
  
  def states
	  @page = Page.find_by_permalink(params[:id])		
    if params[:id]
      @state = State.find_by_name(params[:id])
    end
  end
  
  def category
	  @page = Page.find_by_permalink(params[:id])		
  end

  def reference
    @page = Page.find_by_permalink(params[:id])
  end

  def education
    @page = Page.find_by_permalink(params[:id])
  end

  def populate_state
    if params[:country_id].present?
      @states = CS.states(params[:country_id])
        render :text => view_context.options_for_select(@states.values) 
    end
  end

end
