class HomeController < ApplicationController
  before_filter :load_pages
  def index
  	@homeimages = Homeimage.all
  	@latest_updates = LatestUpdate.where('online is true')
  end
end
