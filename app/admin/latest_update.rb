ActiveAdmin.register LatestUpdate do

menu :priority => 6

permit_params :title, :online, :description, :link

  form do |f|
    f.inputs do
      f.input :title
      f.input :link
      f.input :online
      f.input :description, :as => :ckeditor
    end
    f.actions
  end

  index do
	selectable_column
    id_column
    column :title
    column :link
    column :online
    column :created_at
    column :updated_at
    actions 
  end

end
