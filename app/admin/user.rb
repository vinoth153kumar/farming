ActiveAdmin.register User do
menu :priority => 3

permit_params :email, :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at, :sign_in_count, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip, :confirmation_token, :confirmed_at, :confirmation_sent_at, :unconfirmed_email, :provider, :uid, :name, :dob, :mobile, :address, :state, :country, :created_at, :updated_at,:forem_admin,:forem_state, :forem_auto_subscribe

index do
    selectable_column
    id_column
    column :name
    column :email
    column :dob
    column :mobile
    column :address
    column :state
    column :country
    column :created_at
    column :updated_at
    actions
  end

end