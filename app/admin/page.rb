ActiveAdmin.register Page do

menu :priority => 5

permit_params :title, :content, :permalink,:online

index do
	selectable_column
    id_column
    column :title
    column :permalink
    column :online
    column :created_at
    column :updated_at
  actions
end

  form do |f|
    f.inputs do
      f.input :title
      f.input :permalink
      f.input :online
      f.input :content, :as => :ckeditor
    end
    f.actions
  end

end
