ActiveAdmin.register State do

  menu :priority => 8

  permit_params :name, :title, :online, :content

index do
	selectable_column
    id_column
    column :name
    column :title
    column :online
    column :created_at
    column :updated_at
  actions 
end

  form do |f|
    f.inputs do
      f.input :name, as: :select, collection:  CS.states(:IN).map{|u| [u[1], u[1]]}
      f.input :title
      f.input :online
      f.input :content, :as => :ckeditor
    end
    f.actions
  end

end
