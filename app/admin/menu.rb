ActiveAdmin.register Menu do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :position, :permalink
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  form do |f|
    f.inputs do
      f.input :name
      f.input :position
      f.input :permalink, as: :select, collection: Page.where('online is true').map{|u| [u.permalink, u.permalink]}
    end
    f.actions
  end

end
