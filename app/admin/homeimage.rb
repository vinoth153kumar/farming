ActiveAdmin.register Homeimage do

  menu :priority => 7

  permit_params :image

  index do
	selectable_column
    id_column
    column :image_file_name
    column 'Image' do |f|
    	image_tag(f.image.url(:med)) 
    end
    column :image_content_type
    column :created_at
    column :updated_at
    actions
 end

  form do |f|
    f.inputs do
      f.input 'images',as: :file,input_html: { multiple: true } 
    end
    f.actions
  end

  show do
     attributes_table do
      row :id
      row :admin_user_id
      row :image do |f|
        image_tag(f.image.url(:large)) 
      end
    end
    active_admin_comments
  end

  controller do
  	def create
  	  if params[:homeimage][:images].present?
  	  	params[:homeimage][:images].each do |image|
  	  		current_admin_user.homeimages.create!({image: image })
  	  	end
  	  	flash[:notice] = 'Home images created successfully!'
  	  	redirect_to admin_homeimages_path
  	  else
  	  	flash[:alert] = 'Problem to create!'
  	  	redirect_to admin_homeimages_path
  	  end
  	end

    def update
      if params[:homeimage][:images].count == 1
        @homeimage = Homeimage.find(params[:id])
        @homeimage.update_attributes({image: params[:homeimage][:images].first})
        flash[:notice] = 'Home images updated successfully!'
        redirect_to admin_homeimage_path(@homeimage)
      elsif params[:homeimage][:images].count != 1
        params[:homeimage][:images].each do |image|
          current_admin_user.homeimages.create!({image: image })
        end
        flash[:notice] = 'Home images updated successfully!'
        redirect_to admin_homeimages_path
      else
        flash[:alert] = 'Problem to create!'
        redirect_to admin_homeimages_path
      end
    end

  end

end
