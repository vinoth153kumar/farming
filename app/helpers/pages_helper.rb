module PagesHelper
  def bank_image(link, image)
    content_tag :div, :class => "col-md-3", style: 'padding:0px;' do
      content_tag(:a, "<img alt='Continue' src='/assets/#{image}'>".html_safe, href: "#{link}", target: '_blank')
    end
  end

  def bank_name(link, name)
    content_tag :div, :class => "col-md-9", style: 'padding:0px;' do
      content_tag(:a, "#{name}", href: "#{link}", target: '_blank')
    end
  end
end
