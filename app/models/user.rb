class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :omniauthable, :omniauth_providers => [:facebook]

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.confirmed_at = Time.now.utc
      user.name = auth.info.name
      user.save(validate: false)
    end
  end

  def forem_name
    name
  end

  def forem_email
    email
  end

  validates_presence_of :email, message: 'Email can\'t be blank'
  validates_presence_of :name, message: 'Name can\'t be blank'
  validates_presence_of :dob, message: 'Date of Birth can\'t be blank'
  validates_presence_of :mobile, message: 'Mobile can\'t be blank'
  validates_presence_of :address, message: 'Address can\'t be blank'
  validates_presence_of :state, message: 'State can\'t be blank'
  validates_presence_of :country, message: 'Country can\'t be blank'
  validates_presence_of :password, message: 'Password can\'t be blank'
  

end
