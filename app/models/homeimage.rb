class Homeimage < ActiveRecord::Base
	belongs_to :admin_user
	has_attached_file :image, styles: { small: "64x64", med: "100x100", large: "926x421" }
	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
