class Page < ActiveRecord::Base
  
  has_many :menus
  
  validates_presence_of :permalink
  validates_uniqueness_of :permalink

end
