Rails.application.routes.draw do

  devise_for :users, controllers: {:omniauth_callbacks => "users/omniauth_callbacks", :registrations => "registrations"}

  mount Ckeditor::Engine => '/ckeditor'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  mount Forem::Engine, :at => '/forum'

  root 'home#index'
  resources :pages do
    collection do
      get 'category'
      get 'states'
      get 'reference'
      get 'wheat'
      get 'maize'
      get 'rice'
      get 'pulses'
      get 'jute'
      get 'sugarcane'
      get 'nuts'
      get 'sand'
      get 'seasons'
      get 'debt_relif'
      get 'short_term_loan'
      get 'midloan'
      get 'agri_banking'
      get 'agri_credit'
      get 'populate_state'
      get 'education'
    end
  end
  resources :states

offline = Rack::Offline.configure :cache_interval => 120 do 
  cache ActionController::Base.helpers.asset_path("application.css")
  cache ActionController::Base.helpers.asset_path("application.js")
  cache ActionController::Base.helpers.asset_path("desktop.css")
  cache ActionController::Base.helpers.asset_path("desktopd7b7.css")
  cache ActionController::Base.helpers.asset_path("ipad.css")
  cache ActionController::Base.helpers.asset_path("ipadv.css")
  cache ActionController::Base.helpers.asset_path("phonehorizontal.css")
  cache ActionController::Base.helpers.asset_path("phonevertical.css")
  cache ActionController::Base.helpers.asset_path("themes/alertnate-texture-body-bottom.png")
  cache ActionController::Base.helpers.asset_path("themes/alertnate-texture-body-full-bottom.png")
  cache ActionController::Base.helpers.asset_path("themes/alertnate-texture-body-full-top.png")
  cache ActionController::Base.helpers.asset_path("themes/alertnate-texture-body-full.jpg")
  cache ActionController::Base.helpers.asset_path("themes/alertnate-texture-body-top.png")
  cache ActionController::Base.helpers.asset_path("themes/alertnate-texture-body.jpg")
  cache ActionController::Base.helpers.asset_path("themes/background-overlay.png")
  cache ActionController::Base.helpers.asset_path("themes/background-texture-1.jpg")
  cache ActionController::Base.helpers.asset_path("themes/coupon-texture-1.jpg")
  cache ActionController::Base.helpers.asset_path("themes/content-background.jpg")
  cache ActionController::Base.helpers.asset_path("themes/header-texture-1.jpg")
  cache ActionController::Base.helpers.asset_path("themes/main-menu-texture-1.jpg")
  cache ActionController::Base.helpers.asset_path("themes/coupon-overlay.png")
  cache ActionController::Base.helpers.asset_path("agri-snapshot.png")
  cache ActionController::Base.helpers.asset_path("agri_credit.jpg")
  cache ActionController::Base.helpers.asset_path("debt.jpg")
  cache ActionController::Base.helpers.asset_path("education.jpg")
  cache ActionController::Base.helpers.asset_path("facebook.png")
  cache ActionController::Base.helpers.asset_path("fb-logo.png")
  cache ActionController::Base.helpers.asset_path("growth.jpg")
  cache ActionController::Base.helpers.asset_path("home.jpg")
  cache ActionController::Base.helpers.asset_path("indiarice.png")
  cache ActionController::Base.helpers.asset_path("jute.jpg")
  cache ActionController::Base.helpers.asset_path("loans.jpg")
  cache ActionController::Base.helpers.asset_path("logo.png")
  cache ActionController::Base.helpers.asset_path("maize.jpg")
  cache ActionController::Base.helpers.asset_path("maizeindia.png")
  cache ActionController::Base.helpers.asset_path("midloan.jpg")
  cache ActionController::Base.helpers.asset_path("nuts.jpg")
  cache ActionController::Base.helpers.asset_path("pulses.jpg")
  cache ActionController::Base.helpers.asset_path("rice.jpg")
  cache ActionController::Base.helpers.asset_path("sands.jpg")
  cache ActionController::Base.helpers.asset_path("season.jpg")
  cache ActionController::Base.helpers.asset_path("spices.jpg")
  cache ActionController::Base.helpers.asset_path("sugarcane.jpg")
  cache ActionController::Base.helpers.asset_path("wheat.jpg")


network "/"
end


get "/application.manifest" => offline


end
